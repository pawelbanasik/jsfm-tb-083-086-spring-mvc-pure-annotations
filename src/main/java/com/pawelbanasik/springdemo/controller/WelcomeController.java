package com.pawelbanasik.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pawelbanasik.springdemo.service.GenericWelcomeService;

@Controller
public class WelcomeController {

	@Autowired
	private GenericWelcomeService welcomeService;

	// moge wyrzucic slash bo mam juz mapping ale nie musze bo nic to nie zmieni
	@RequestMapping("/")
	public String doWelcome(Model model) {

		List<String> welcomeMessage = welcomeService.getWelcomeMessage("Pawel Banasik");
		model.addAttribute("myWelcomeMessage", welcomeMessage);
		return "welcomeNew";
	}
}
